import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const Home  = {
    template: '<div>Home</div>'
};



const About = {
    template: '<div>About</div>'
};

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: Home },
        { path: '/about', component: About },
        {
            name: '404',
            path: '/404',
            component: {
                template: "<div>404</div>",
            },
        },
        // low priority otherwise
        {
            name: 'otherwise',
            path: '/(.*)',
            redirect: {name: '404'}
        },
    ]
});

export default router;