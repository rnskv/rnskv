import Vue from 'vue';
import store from './Store.js';
import VueSocketIO from './plugins/socket.io/vue.plugin.js';
import router from './Router.js';

Vue.config.devtools = true;
Vue.use(VueSocketIO, {connect: 'http://localhost:8081'});

import ScreenContainer from './containers/ScreenContainer.vue'

let app = new Vue({
    el: '#app',
    store,
    router,
    render: h => h(ScreenContainer)
});

window.app = app;

// Коннектимся к web socket
// app.$socket.connect();

// Пример запроса к веб сокетам с полученим ответа
